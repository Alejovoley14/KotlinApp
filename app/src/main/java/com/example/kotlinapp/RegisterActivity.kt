package com.example.kotlinapp

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private lateinit var txtNombre: EditText
    private lateinit var txtApellido: EditText
    private lateinit var txtEmail: EditText
    private lateinit var txtPass: EditText
    private lateinit var progrssBar: ProgressBar
    private lateinit var dbReference: DatabaseReference
    private lateinit var database: FirebaseDatabase
    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        txtNombre = findViewById(R.id.nombreRegisterInput)
        txtApellido = findViewById(R.id.apellidoRegisterInput)
        txtEmail = findViewById(R.id.emailRegisterInput)
        txtPass = findViewById(R.id.passRegisterInput)

        progrssBar = findViewById(R.id.progressBarRegister)

        //Intancias
        database = FirebaseDatabase.getInstance()
        auth = FirebaseAuth.getInstance()

        dbReference = database.reference.child("Usuario")

        loginBtn.setOnClickListener {
            registrarCuentaNueva()
        }
    }

    private fun registrarCuentaNueva() {

        val nombre: String = txtNombre.text.toString()
        val apellido: String = txtApellido.text.toString()
        val email: String = txtEmail.text.toString()
        val pass: String = txtPass.text.toString()

        if (!TextUtils.isEmpty(nombre) && !TextUtils.isEmpty(apellido) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(
                pass
            )
        ) {
            progrssBar.visibility = View.VISIBLE

            auth.createUserWithEmailAndPassword(email, pass)
                .addOnCompleteListener(this) { // Esta metodo es para verificar si la accion se realizo correctamente

                        task ->

                    if (task.isComplete) {
                        val user: FirebaseUser? = auth.currentUser
                        verificarEmail(user)

                        val userDb = dbReference.child(user?.uid!!)

                        userDb.child("Nombre").setValue(nombre)
                        userDb.child("Apellido").setValue(apellido)
                        userDb.child("Email").setValue(email)

                        actionSuccess()
                    }
                }
        }

    }

    private fun actionSuccess() {
        startActivity(Intent(this, LoginActivity::class.java))

    }

    private fun verificarEmail(user: FirebaseUser?) {

        user?.sendEmailVerification()
            ?.addOnCompleteListener(this) { task ->

                if (task.isComplete) {
                    Toast.makeText(this, "Email Enviado", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this, "Error al enviar el email", Toast.LENGTH_LONG).show()
                }
            }
    }

}
