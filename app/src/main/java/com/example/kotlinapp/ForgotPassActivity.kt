package com.example.kotlinapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.AbsListView
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class ForgotPassActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var txtEmail: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_pass)

        txtEmail = findViewById(R.id.forgotPassEmail)
        auth = FirebaseAuth.getInstance()
    }

    fun sendForgotPass (view: View){

        var email = txtEmail.text.toString()

        if (!TextUtils.isEmpty(email)){

            auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(this){
                task ->

                    if (task.isSuccessful){
                        startActivity(Intent(this, LoginActivity::class.java))
                    }
                    else{
                        Toast.makeText(this, "Error al cambiar la contraseña", Toast.LENGTH_LONG).show()
                    }

            }
        }
    }
}
