package com.example.kotlinapp

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    private lateinit var txtEmail: EditText
    private lateinit var txtPass: EditText
    private lateinit var auth: FirebaseAuth
    private lateinit var progrssBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.sleep(1000)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        txtEmail = findViewById(R.id.forgotPassEmail)
        txtPass = findViewById(R.id.passLoginInput)
        auth = FirebaseAuth.getInstance()
        progrssBar = findViewById(R.id.progressBarLogin)


    }

    fun loguear(view: View) {
        loguearUsuario()
    }

    private fun loguearUsuario() {

        val email: String = txtEmail.text.toString()
        val pass: String = txtPass.text.toString()

        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(pass)) {
            progrssBar.visibility = View.VISIBLE

            auth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(this) {

                        task ->

                    if (task.isSuccessful) {
                        actionHome()
                    } else {
                        Toast.makeText(this, "Error al loguear usuario", Toast.LENGTH_LONG).show()
                    }
                }
        }

    }
    fun actionRegistrar(view: View) {
        startActivity(Intent(this, RegisterActivity::class.java))
    }

    fun actionForgotPass(view: View) {
        startActivity(Intent(this, ForgotPassActivity::class.java))
    }

    private fun actionHome() {
        startActivity(Intent(this, HomeActivity::class.java))
    }

}
